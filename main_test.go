package main

import (
    "io"
    "net/http/httptest"
    "regexp"
    "testing"
)

func init() {
    var _ = func() bool {
        testing.Init()
        return true
    }()
}

func TestIcmp(t *testing.T) {
    t.Run("Test that ping with icmp protocol to collect latency metric in parallel", func(t *testing.T) {
        t.Parallel()

        req := httptest.NewRequest("GET", "/ping", nil)

        qry := req.URL.Query() // or url.Values{}
        qry.Add("target", "127.0.0.1")
        qry.Add("protocol", "icmp")
        qry.Add("counts", "1")
        qry.Add("interval", "1")
        qry.Add("timeout", "1")
        qry.Add("collect_type", "latency")
        req.URL.RawQuery = qry.Encode()

        w := httptest.NewRecorder()
        pingHandler(w, req)

        resp := w.Result()
        defer resp.Body.Close()
        body, _ := io.ReadAll(resp.Body)

        valid := regexp.MustCompile("pingLatency_Time")

        if ! valid.Match(body) {
            t.Errorf("Unit Test (Metric Response Error) Fail\n")
        }
    })

    t.Run("Test that ping with icmp protocol to collect statistics metrics in parallel", func(t *testing.T) {
        t.Parallel()

        req := httptest.NewRequest("GET", "/ping", nil)

        qry := req.URL.Query() // or url.Values{}
        qry.Add("target", "127.0.0.1")
        qry.Add("protocol", "icmp")
        qry.Add("counts", "1")
        qry.Add("interval", "1")
        qry.Add("timeout", "1")
        qry.Add("collect_type", "statistics")
        req.URL.RawQuery = qry.Encode()

        w := httptest.NewRecorder()
        pingHandler(w, req)

        resp := w.Result()
        defer resp.Body.Close()
        body, _ := io.ReadAll(resp.Body)

        valid := regexp.MustCompile("pingLatency_PacketLoss")

        if ! valid.Match(body) {
            t.Errorf("Unit Test (Metric Response Error) Fail\n")
        }
    })
}

func TestUdp(t *testing.T) {
    t.Run("Test that ping with udp protocol to collect latency metric in parallel", func(t *testing.T) {
        t.Parallel()

        req := httptest.NewRequest("GET", "/ping", nil)

        qry := req.URL.Query() // or url.Values{}
        qry.Add("target", "127.0.0.1")
        qry.Add("protocol", "udp")
        qry.Add("counts", "1")
        qry.Add("interval", "1")
        qry.Add("timeout", "1")
        qry.Add("collect_type", "latency")
        req.URL.RawQuery = qry.Encode()

        w := httptest.NewRecorder()
        pingHandler(w, req)

        resp := w.Result()
        defer resp.Body.Close()
        body, _ := io.ReadAll(resp.Body)

        valid := regexp.MustCompile("pingLatency_Time")

        if ! valid.Match(body) {
            t.Errorf("Unit Test (Metric Response Error) Fail\n")
        }
    })

    t.Run("Test that ping with udp protocol to collect statistics metrics in parallel", func(t *testing.T) {
        t.Parallel()

        req := httptest.NewRequest("GET", "/ping", nil)

        qry := req.URL.Query() // or url.Values{}
        qry.Add("target", "127.0.0.1")
        qry.Add("protocol", "udp")
        qry.Add("counts", "1")
        qry.Add("interval", "1")
        qry.Add("timeout", "1")
        qry.Add("collect_type", "statistics")
        req.URL.RawQuery = qry.Encode()

        w := httptest.NewRecorder()
        pingHandler(w, req)

        resp := w.Result()
        defer resp.Body.Close()
        body, _ := io.ReadAll(resp.Body)

        valid := regexp.MustCompile("pingLatency_PacketLoss")

        if ! valid.Match(body) {
            t.Errorf("Unit Test (Metric Response Error) Fail\n")
        }
    })
}
