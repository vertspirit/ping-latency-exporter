package main

import (
    "context"
    "flag"
    "fmt"
    "io"
    "log"
    "log/slog"
    "log/syslog"
    "net/http"
    "os"
    "os/signal"
    "syscall"
    "testing"
    "time"

    "github.com/prometheus/client_golang/prometheus/promhttp"
    "gopkg.in/natefinch/lumberjack.v2"
    "github.com/aws/aws-lambda-go/lambda"
    "github.com/awslabs/aws-lambda-go-api-proxy/httpadapter"
)

var (
    Version      string
    Build        string
    serveMode    string
    serverIP     string
    serverPort   int
    graceTimeout int
    logParam     bool
    fileLog      bool
    logFile      string
    maxSize      int      // in megabytes
    maxAge       int      // in days
    maxBackups   int
    logLocaltime bool
    logCompress  bool
)

const (
    htmlContent string = `<html>
      <head>
      <title>Ping Latency Exporter</title>
      <style>
          label{
              display:inline-block;
              width:80px;
          }
          form label {
              margin: 10px;
          }
          form input {
              margin: 10px;
          }
      </style>
      </head>
      <body>
      <h2>Ping Latency Exporter</h2>
      <form action="/ping">
      <label>Target:</label> <input type="text" name="target" placeholder="IPv4 or IPv6 Address or hostname" value="linux.org"><br>
      <label>Protocol:</label> <input type="text" name="protocol" placeholder="icmp or udp" value="icmp"><br>
      <label>Network (Optional):</label> <input type="text" name="network" placeholder="ip, ip4 or ip6" value=""><br>
      <label>Count:</label> <input type="text" name="counts" placeholder="1" value="1"><br>
      <label>Interval:</label> <input type="text" name="interval" placeholder="1" value="1"><br>
      <label>Timeout:</label> <input type="text" name="timeout" placeholder="3" value="3"><br>
      <label>Collect Type:</label> <input type="text" name="collect_type" placeholder="latency or statistics" value="statistics"><br>
      <label>Source Address (Optional):</label> <input type="text" name="source" placeholder="ip to use" value=""><br>
      <label>Source Interface (Optional):</label> <input type="text" name="nic" placeholder="nic name to use" value=""><br>
      <input type="submit" value="Submit">
      </form>
          <p><!---<a href="/config">Config</a>---></p>
      </body>
      </html>`
)

func init() {
    testing.Init()

    flag.StringVar(&serverIP, "server", "", "The IP address of the metrics gathering service.")
    flag.StringVar(&serverIP, "s", "", "The IP address of the metrics gathering service. (shorten)")
    flag.IntVar(&serverPort, "port", 1998, "The port of the metrics gathering service.")
    flag.IntVar(&serverPort, "p", 1998, "The port of the metrics gathering service. (shorten)")
    flag.IntVar(&graceTimeout, "graceful-timeout", 3, "The timeout for server graceful shutdown.")
    flag.IntVar(&graceTimeout, "g", 3, "The timeout for server graceful shutdown.")
    flag.BoolVar(&logParam, "syslog", true, "Enbale the logging to the syslog.")
    flag.BoolVar(&logParam, "l", true, "Enbale that logging to the syslog. (shorten)")
    flag.BoolVar(&fileLog, "filelog", false, "Enable that logging to a file.")
    flag.BoolVar(&fileLog, "f", false, "Enable that logging to a file. (shorten)")
    flag.StringVar(
        &logFile,
        "logfile",
        "/var/log/ping-latency-exporter/ping-latency-exporter.log",
        "The file to log into, default is /var/log/ping-latency-exporter/ping-latency-exporter.log.",
    )
    flag.BoolVar(&logLocaltime, "log-localtime", true, "Logging with localtime in file.")
    flag.BoolVar(&logCompress, "log-compress", true, "Compress log file.")
    flag.IntVar(&maxSize, "log-max-size", 10, "The max size of log file to rorate in megabytes, default is 10MB.")
    flag.IntVar(&maxAge, "log-max-age", 7, "How long to rorate current log file in days, default is 7 days.")
    flag.IntVar(&maxBackups, "log-max-backups", 5, "How many files to retain after rorate, default is 5.")
    flag.StringVar(&serveMode, "mode", "default", "The mode of server which could be standard or lambda, default is standard.")
    flag.StringVar(&serveMode, "m", "default", "The mode of server which could be standard or lambda, default is standard. (shorten)")
    flag.Parse()

    log.SetPrefix("[ping-latency-exporter] ")
    log.SetFlags(log.LstdFlags|log.Lshortfile)

    mlw := io.MultiWriter(os.Stdout)

    flog := &lumberjack.Logger {
        Filename:   logFile,
        MaxSize:    maxSize,
        MaxAge:     maxAge,
        MaxBackups: maxBackups,
        Compress:   logLocaltime,
        LocalTime:  logLocaltime,
    }
    if logParam {
        logger, err := syslog.New(syslog.LOG_ERR|syslog.LOG_DAEMON, "[ping-latency-exporter] ")
        if err == nil {
            if fileLog {
                mlw = io.MultiWriter(os.Stdout, logger, flog)
            } else {
                mlw = io.MultiWriter(os.Stdout, logger)
            }
        } else {
            slog.Error(err.Error())
        }
    } else {
        if fileLog {
            mlw = io.MultiWriter(os.Stdout, flog)
        }
    }

    slogger := slog.New(slog.NewJSONHandler(mlw, nil).WithAttrs(
        []slog.Attr{slog.String("app", "ping-latency-exporter")},
    ))
    slog.SetDefault(slogger)
}

func main() {
    http.Handle("/metrics", promhttp.Handler())
    http.HandleFunc("/ping", pingHandler)
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        w.Write([]byte(htmlContent))
    })

    addr := fmt.Sprintf("%s:%d", serverIP, serverPort)
    fmt.Printf("+++ Version: %s\n", Version)
    fmt.Printf("+++ Build Date: %s\n", Build)

    switch serveMode {
    case "lambda":
        lambda.Start(httpadapter.New(http.DefaultServeMux).ProxyWithContext)
        return
    default:
        srv := &http.Server {
            Addr:           addr,
        }

        go func() {
            if err := srv.ListenAndServe(); err != nil {
                slog.Error(fmt.Sprintf("Server Listen Error: %v", err))
            }
        }()
        fmt.Printf("+++ Beginning to serve on '%s'\n", addr)
    
        quit := make(chan os.Signal, 1)
        signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
        <-quit
        slog.Info("Shutting down server...")

        ctx, cancel := context.WithTimeout(
            context.Background(),
            time.Duration(graceTimeout)*time.Second,
        )
        defer cancel()
        if err := srv.Shutdown(ctx); err != nil {
            errorMessage := fmt.Sprintf("Server forced to shutdown: %v", err)
            slog.Error(errorMessage)
            os.Exit(1)
        }
        select {
        case <-ctx.Done():
            slog.Info("Graceful shutdown start...")
            close(quit)
        }
        slog.Info("Graceful shutdown finished...")
    }
}
