BINARY_NAME   = ping-latency-exporter
VERSION       = 1.0.1
BUILD_DATE    = $(shell date +%F)
BUILD_FLAGS   = "-X main.Version=${VERSION} -X main.Build=${BUILD_DATE}"
TZ            = Asia/Taipei
GO_VERSION    = 1.22
CURRENT_DIR   = $(shell pwd)
REBUILD       = 1
DEBIAN_VER    = 12
UBN_VERSION   = 22.04
CENTOS_VER    = 7
COVERAGE_FILE =

.PHONY: build
build:
	GOARCH=amd64 CGO_ENABLED=0 \
	go build -mod mod -buildvcs=false -ldflags ${BUILD_FLAGS} \
	-o bin/${BINARY_NAME}

run:
	go mod tidy
	go run -race -mod mod -buildvcs=false -ldflags ${BUILD_FLAGS} . -syslog=false

clean:
	go clean
	go clean -testcache
	@if [ -d bin ] ; then rm -rf bin ; fi
	@if [ -d packages ] ; then rm -rf packages ; fi

docker:
	docker buildx build --no-cache \
	--build-arg "TZ=${TZ}" \
	--build-arg "GO_VERSION=${GO_VERSION}" \
	--build-arg "SERVICE_VERSION=${VERSION}" \
	--tag "${BINARY_NAME}:${VERSION}" \
	--tag "${BINARY_NAME}:latest" \
	--file build/Dockerfile \
	.

lambda:
	docker buildx build --no-cache \
	--build-arg "TZ=${TZ}" \
	--build-arg "GO_VERSION=${GO_VERSION}" \
	--build-arg "SERVICE_VERSION=${VERSION}" \
	--tag "${BINARY_NAME}-lambda:${VERSION}" \
	--tag "${BINARY_NAME}-lambda:latest" \
	--file build/Dockerfile-lambda \
	.

lint:
	golangci-lint run --verbose

benchmark:
	go mod tidy
	go test -bench=.

.PHONY: test
test:
	go mod tidy
	@if [ ! -z ${COVERAGE_FILE} ] ; then go test -race -v -run TestIcmp "-coverprofile=${COVERAGE_FILE}" -covermode atomic ; fi
	@if [ -z ${COVERAGE_FILE} ] ; then go test -race -v -run TestUdp -covermode atomic ; fi
	@# go test -race -v ./...

debian:
	mkdir -p tmp
	cp *.* tmp/
	cp -r build tmp/
	cp -r scripts tmp/
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/debian-nfpm:${DEBIAN_VER} chown -R geek .
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/debian-nfpm:${DEBIAN_VER} go build -mod mod -buildvcs=false -ldflags ${BUILD_FLAGS} -o bin/${BINARY_NAME}
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/debian-nfpm:${DEBIAN_VER} yq -i '.version = "${VERSION}"' build/nfpm.yaml
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/debian-nfpm:${DEBIAN_VER} nfpm pkg --packager deb --config build/nfpm.yaml --target "${BINARY_NAME}_${VERSION}-${REBUILD}_debian_amd64.deb"
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/debian-nfpm:${DEBIAN_VER} chown -R $(shell id -u) .
	@if [ ! -d packages ] ; then mkdir -p packages ; fi
	mv tmp/${BINARY_NAME}_${VERSION}-${REBUILD}_debian_amd64.deb packages/
	rm -rf tmp/

ubuntu:
	mkdir -p tmp
	cp *.* tmp/
	cp -r build tmp/
	cp -r scripts tmp/
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/ubuntu-nfpm:${UBN_VERSION} chown -R geek .
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/ubuntu-nfpm:${UBN_VERSION} go build -mod mod -buildvcs=false -ldflags ${BUILD_FLAGS} -o bin/${BINARY_NAME}
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/ubuntu-nfpm:${UBN_VERSION} yq -i '.version = "${VERSION}"' build/nfpm.yaml
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/ubuntu-nfpm:${UBN_VERSION} nfpm pkg --packager deb --config build/nfpm.yaml --target "${BINARY_NAME}_${VERSION}-${REBUILD}_ubuntu_amd64.deb"
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/ubuntu-nfpm:${UBN_VERSION} chown -R $(shell id -u) .
	@if [ ! -d packages ] ; then mkdir -p packages ; fi
	mv tmp/${BINARY_NAME}_${VERSION}-${REBUILD}_ubuntu_amd64.deb packages/
	rm -rf tmp/

centos:
	mkdir -p tmp
	cp *.* tmp/
	cp -r build tmp/
	cp -r scripts tmp/
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/centos-nfpm:${CENTOS_VER} chown -R geek .
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/centos-nfpm:${CENTOS_VER} go build -mod mod -buildvcs=false -ldflags ${BUILD_FLAGS} -o bin/${BINARY_NAME}
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/centos-nfpm:${CENTOS_VER} yq -i '.version = "${VERSION}"' build/nfpm.yaml
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp \
	ghcr.io/vertspirit/centos-nfpm:${CENTOS_VER} nfpm pkg --packager rpm --config build/nfpm.yaml --target "${BINARY_NAME}-${VERSION}-${REBUILD}-x86_64.rpm"
	docker run --rm -v ${CURRENT_DIR}/tmp:/opt/tmp --workdir /opt/tmp -u root \
	ghcr.io/vertspirit/centos-nfpm:${CENTOS_VER} chown -R $(shell id -u) .
	@if [ ! -d packages ] ; then mkdir -p packages ; fi
	mv tmp/${BINARY_NAME}-${VERSION}-${REBUILD}-x86_64.rpm packages/
	rm -rf tmp/

help:
	@echo "make build VERSION=1.0.1 - compile the binary file with golang codes"
	@echo "make docker VERSION=1.0.1 GO_VERSION=1.22 - create the docker image from build/Dockerfile"
	@echo "make lambda VERSION=1.0.1 GO_VERSION=1.22 - create the lambda version image from build/Dockerfile-lambda"
	@echo "make debian VERSION=1.0.1 - build deb package of debian by nfpm"
	@echo "make ubuntu VERSION=1.0.1 - build deb package of ubuntu by nfpm"
	@echo "make centos VERSION=1.0.1 - build rpm package by nfpm for centos"
	@echo "make clean - remove the binary file in the bin directory"
	@echo "make lint - check golang syntax"
	@echo "make benchmark - run benchmark"
	@echo "make test COVERAGE_FILE=coverage.txt - run test with -race parameter"
	@echo "make run VERSION=1.0.1 - run the program with '-race' and '-covermode atomic' parameter"
