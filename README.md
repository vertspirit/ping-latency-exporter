# Ping Latency Exporter

A simple ping latency exporter written in golang to ping target and gather the metrics of RTT and packet loss by [github.com/prometheus-community/pro-bing](https://github.com/prometheus-community/pro-bing).

## Build

Install the golang and git package:

```bash
# Ubuntu
sudo snap install go && sudo apt-get -y install git

# CentOS
yum -y install make golang git
```

Download the source from gitlab.com:

```bash
git clone https://gitlab.com/vertspirit/ping-latency-exporter
```

Run the following command to build binary which placed in the bin/ folder after compiled successfully:

```bash
make build VERSION=1.0.1
```

Run the following command to build docker image:

```bash
make docker VERSION=1.0.1 GO_VERSION=1.22
```

Run the following command to build aws lambda version image:

```bash
make lambda VERSION=1.0.1 GO_VERSION=1.22
```

Run the following command to build deb package for debian:

```bash
make debian VERSION=1.0.1
```

You could build deb package for ubuntu as well, just run the following command:

```bash
make ubuntu VERSION=1.0.1
```

To build rpm package, run the following command:

```bash
make centos VERSION=1.0.1
```

## ICMP on Linux

Run setcap for the ping-latency-exporter binary to allow bind raw sokcets with non-root user:

```bash
sudo setcap cap_net_raw=+ep /path/to/ping-latency-exporter/binary
```

## AWS Lambda Mode

The ping-latency-exporter could run in aws lambda mode as cloud serverless service. Use the 'mode' parameter to switch:

```bash
ping-latency-exporter -mode lambda
```

Build the docker image for RIE (aws-lambda-runtime-interface-emulator):

```bash
docker build --tag ping-latency-exporter-lambda --file build/Dockerfile-lambda .
```

Run the lambda version docker image with RIE (8080 port):

```bash
docker run -t -d \
    -p 9000:8080 \
    --name ping-latency-exporter-lambda \
    ping-latency-exporter-lambda:latest
```

Send request to test lambda function with test-data/lambda-rie-request.json by RIE (start RIE autimatically without given AWS_LAMBDA_RUNTIME_API and \_LAMBDA_SERVER_PORT environment variables):

```bash
curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d @test-data/lambda-rie-request.json
```

The following is a simple example for lambda-rie-request.json:

```json
{
    "httpMethod": "GET",
    "path": "/ping",
    "queryStringParameters": {
        "target": "linux.org",
        "protocol": "icmp",
        "counts": "1",
        "interval": "1",
        "timeout": "1",
        "collect_type": "statistics"
    },
    "headers": {
        "accept": "text/html,application/xhtml+xml",
        "accept-language": "en-US,en;q=0.8",
        "content-type": "text/plain",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)"
    },
    "isBase64Encoded": false,
    "body": "request_body"
}
```

## Prometheus Config

### Collect Type

You could set collect type to determine the metrics of reponse. And there are three types that include all, 'all', 'latency' and 'statistics'.

The following is the result example after run ping command usually:

```
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=56 time=5.50 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=56 time=4.78 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=56 time=4.54 ms

--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 4.540/4.938/5.495/0.405 ms
```

The 'latency' type only response the metric about time to take, and 'statistics' type show the statistics part above. Or you could set to 'all' type to show both.

### Config Examples

The example of ping-latency-exporter for the prometheus config (the protocol supports 'udp' or 'icmp'):

```yaml
- job_name: 'ping-exporter'
  scrape_interval: 1s
  metrics_path: /ping
  params:
    protocol: 'icmp'
    counts: [1]
    interval : [1]
    timeout: [1]
    collect_type: 'latency'
  static_configs:
    - targets:
      - 'linux.org'
      - 'gitlab.com'
      - '1.1.1.1'
```

or with file_sd_configs, such as:

```yaml
- job_name: 'ping-exporter'
    scrape_interval: 1s
    metrics_path: /ping
    params:
      protocol: 'icmp'
      counts: [1]
      interval : [1]
      timeout: [1]
      collect_type: 'latency'
    file_sd_configs:
      - files:
        - /etc/prometheus/prometheus.d/ping-targets.yml
        refresh_interval: 5m
```

and ping-targets.yml file like this:

```yaml
- targets:
  - 'linux.org'
  - 'gitlab.com'
  - '1.1.1.1'
  labels:
    job: ping-latency
    service: network-monitoring
```

Collect the statistics metrics with 'statistics' as collect_type, or change to 'all' show both:

```yaml
- job_name: 'ping-exporter'
  scrape_interval: 5s
  metrics_path: /ping
  params:
    protocol: 'icmp'
    counts: [3]
    interval : [1]
    timeout: [1]
    collect_type: 'statistics'
  static_configs:
    - targets:
      - 'linux.org'
      - 'gitlab.com'
      - '1.1.1.1'
```

Set the 'source' or 'nic' to ping targets, and ping-latency-exporter will look up first ip address of nic to ping target if you set the nic instead of source address.
The 'source' field priority is higher than nic when you set both.

The following example uses 'source' field to set source address to ping:

```yaml
- job_name: 'ping-exporter'
  scrape_interval: 1s
  metrics_path: /ping
  params:
    protocol: 'icmp'
    counts: [1]
    interval : [1]
    timeout: [1]
    collect_type: 'all'
    source: 'IP_TO_USE'
  static_configs:
    - targets:
      - 'linux.org'
      - 'gitlab.com'
      - '1.1.1.1'
```

This uses 'nic' to set source address by network interface, such as eth1:

```yaml
- job_name: 'ping-exporter'
  scrape_interval: 1s
  metrics_path: /ping
  params:
    protocol: 'icmp'
    counts: [1]
    interval : [1]
    timeout: [1]
    collect_type: 'all'
    nic: 'NIC_NAME_TO_USE'
  static_configs:
    - targets:
      - 'linux.org'
      - 'gitlab.com'
      - '1.1.1.1'
```

## Unit Test

Run the following command for unit testing (with udp protocol only):

```bash
make test
```

You could run all test as well (set the permission for icmp to bind raw socket on linux first):

```bash
go test -race -v ./...
```
