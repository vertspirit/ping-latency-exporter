package main

import (
    "fmt"
    "log/slog"
    "net/http"
    "runtime"
    "strconv"
    "time"
    "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)

func pingHandler(w http.ResponseWriter, r *http.Request) {
    target := r.URL.Query().Get("target")
    if target == "" {
        http.Error(w, "'target' parameter must be specified", 400)
        return
    }

    counts, err := strconv.Atoi(r.URL.Query().Get("counts"))
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }
    if counts <= 0 {
        counts = 1
    }

    interval, err := strconv.Atoi(r.URL.Query().Get("interval"))
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }
    if interval <= 0 {
        interval = 1
    }
    interval_sec := time.Duration(interval)

    timeout, err := strconv.Atoi(r.URL.Query().Get("timeout"))
    if err != nil {
        http.Error(w, err.Error(), 400)
        return
    }
    if timeout <= 0 {
        timeout = 10
    }
    timeout_msec := time.Duration(timeout)

    protocol := r.URL.Query().Get("protocol")
    nw := r.URL.Query().Get("network")

    source := r.URL.Query().Get("source")
    nic := r.URL.Query().Get("nic")
    collectType := r.URL.Query().Get("collect_type")

    start := time.Now()
    registry := prometheus.NewRegistry()
    collectorObject := collector {
        CollectorOS: runtime.GOOS,
        Target:      target,
        Interval:    interval_sec,
        Timeout:     timeout_msec,
        Count:       counts,
        Protocol:    protocol,
        Network:     nw,
        Source:      source,
        NicName:     nic,
        CollectType: collectType,
    }

    registry.MustRegister(collectorObject)

    http_serv := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
    http_serv.ServeHTTP(w, r)
    duration := time.Since(start).Seconds()
    slog.Info(fmt.Sprintf("Scrape of target '%s' with interval '%d' and counts '%d' that took %f seconds.\n", target, interval, counts, duration))
}
