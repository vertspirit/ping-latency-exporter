package main

import (
    "fmt"
    "log/slog"
    "net"
    "strconv"
    "sync"
    "time"

    probing "github.com/prometheus-community/pro-bing"
    "github.com/prometheus/client_golang/prometheus"
)

var (
    statisticsMetrics = map[string]string {
        "pingLatency_AvgRtt":                "The Round Trip Time (RTT), the average duration measured in milliseconds",
        "pingLatency_MinRtt":                "The Round Trip Time (RTT), the minimum duration measured in milliseconds",
        "pingLatency_MaxRtt":                "The Round Trip Time (RTT), the maximum duration measured in milliseconds",
        "pingLatency_StdDevRtt":             "The Round Trip Time (RTT), the standard deviation duration measured in milliseconds",
        "pingLatency_PacketsTransmitted":    "The packets transmitted for the icmp ping",
        "pingLatency_PacketsReceived":       "The packets received for the icmp ping",
        "pingLatency_PacketLoss":            "The packets to loss for the icmp ping",
        "pingLatency_PacketsRecvDuplicates": "The number of duplicate responses",
    }
)

type collector struct {
    CollectorOS  string
    Target       string
    Protocol     string
    Network      string
    Source       string
    NicName      string
    CollectType  string
    Count        int
    Interval     time.Duration
    Timeout      time.Duration
}

type metricData struct {
    Entry       string
    Help        string
    Labels      map[string]string
    Stats       *probing.Statistics
}

func (c collector) Describe(ch chan<- *prometheus.Desc) {
    ch <- prometheus.NewDesc("dummy", "dummy", nil, nil)
}

func (c collector) Collect(ch chan<- prometheus.Metric) {
    pinger, err := probing.NewPinger(c.Target)
    if err != nil {
        slog.Error("Pinger Error: %v\n", err)
    }

    switch c.Network {
    case "ip4":
        pinger.SetNetwork("ip4")
    case "ip6":
        pinger.SetNetwork("ip6")
    default:
        pinger.SetNetwork("ip")
    }

    pinger.Count = c.Count
    pinger.Interval = c.Interval * time.Second
    pinger.Timeout = c.Timeout * time.Second

    if c.CollectorOS == "windows" || c.Protocol == "icmp" {
        pinger.SetPrivileged(true) // to avoid windows socket issue, and protocol is udp if not set privileged
    }

    if c.Source != "" {
        // Set ip address to ping, and this is same as ping command with '-S' parameter
        pinger.Source = c.Source
    }

    if c.Source == "" && c.NicName != "" {
        nic, err := net.InterfaceByName(c.NicName)
        if err != nil {
            slog.Error("Network Interface Error: %v\n", err)
        }
        addrs, err := nic.Addrs()
        if err != nil {
            slog.Error("Get Addresses of Network Interface Error: %v\n", err)
            return
        }
        if len(addrs) > 0 {
            pinger.Source = addrs[0].(*net.IPNet).IP.String()
        }
    }

    var labels = map[string]string {
        "address":  c.Target,
        "protocol": c.Protocol,
        "ttl":      strconv.Itoa(pinger.TTL),
        "size":     strconv.Itoa(pinger.Size),
    }

    if c.CollectorOS == "windows" {
        labels["protocol"] = "icmp"
    }

    if pinger.Source != "" {
        labels["from"] = pinger.Source
    }

    pinger.OnRecv = func(pkt *probing.Packet) {
        labels["icmp_seq"] = strconv.Itoa(pkt.Seq)
        if c.CollectType == "latency" || c.CollectType == "all" {
            metric := createMetric(
                "gauge",
                "pingLatency_Time",
                "How long did receiving response take",
                float64(pkt.Rtt * time.Millisecond),
                labels,
            )
            ch <- metric
        }
    }
    pinger.OnDuplicateRecv = func(pkt *probing.Packet) {
        labels["recv"] = "duplicate"
        labels["icmp_seq"] = strconv.Itoa(pkt.Seq)
        if c.CollectType == "latency" || c.CollectType == "all" {
            metric := createMetric(
                "gauge",
                "pingLatency_Time",
                "How long did receiving response take",
                float64(pkt.Rtt * time.Millisecond),
                labels,
            )
            ch <- metric
        }
    }
    pinger.OnFinish = func(stats *probing.Statistics) {
        if c.CollectType != "latency" {
            wg := &sync.WaitGroup{}
            for item, help := range statisticsMetrics {
                wg.Add(1)
                go parseStatisticsMetrics(
                    &metricData {
                        Entry:  item,
                        Help:   help,
                        Labels: labels,
                        Stats:  stats,
                    },
                    ch,
                    wg,
                )
            }
            wg.Wait()
        }
    }

    err = pinger.Run()
    if err != nil {
        slog.Error("Ping Error: %v\n", err)
        return
    }
    // stats := pinger.Statistics()
}

func createMetric(metricType, metricName, metricHelp string, value float64, labels map[string]string) prometheus.Metric {
    metric_type := prometheus.UntypedValue

    labelName := make([]string, 0, len(labels)+1)
    labelValue := make([]string, 0, len(labels)+1)
    for key, val := range labels {
        labelName = append(labelName, key)
        labelValue = append(labelValue, val)
    }

    switch metricType {
    case "counter":
        metric_type = prometheus.CounterValue
    case "gauge":
        metric_type = prometheus.GaugeValue
    case "Float", "Double":
        metric_type = prometheus.GaugeValue
    default:
        metric_type = prometheus.GaugeValue
    }

    gather, err := prometheus.NewConstMetric(prometheus.NewDesc(metricName, metricHelp, labelName, nil), metric_type, value, labelValue...)
    if err != nil {
        gather = prometheus.NewInvalidMetric(
            prometheus.NewDesc("collect_error", "Error calling NewConstMetric",
            nil,
            nil,
        ),
        fmt.Errorf("error for metric %s with labels %v: %v", metricName, labelValue, err))
    }
    return gather
}

func parseStatisticsMetrics(md *metricData, ch chan<- prometheus.Metric, wg *sync.WaitGroup) {
    defer wg.Done()

    var val float64

    switch md.Entry {
    case "pingLatency_AvgRtt":
        val = float64(md.Stats.AvgRtt * time.Millisecond)
    case "pingLatency_MinRtt":
        val = float64(md.Stats.MinRtt * time.Millisecond)
    case "pingLatency_MaxRtt":
        val = float64(md.Stats.MaxRtt * time.Millisecond)
    case "pingLatency_StdDevRtt":
        val = float64(md.Stats.StdDevRtt * time.Millisecond)
    case "pingLatency_PacketsTransmitted":
        val = float64(md.Stats.PacketsSent)
    case "pingLatency_PacketsReceived":
        val = float64(md.Stats.PacketsRecv)
    case "pingLatency_PacketLoss":
        val = md.Stats.PacketLoss
    case "pingLatency_PacketsRecvDuplicates":
        val = float64(md.Stats.PacketsRecvDuplicates)
    }

    metric := createMetric("gauge", md.Entry, md.Help, val, md.Labels)
    ch <- metric
}
